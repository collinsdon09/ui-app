import React, { useRef, useState } from "react";
import {
  Button,
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  Radio,
  RadioGroup,
  Stack,
  Text,
  Select,
  Box,
  ButtonSpinner,
  Heading,
  SimpleGrid,
  HStack,
} from "@chakra-ui/react";

import { Card, CardHeader, CardBody, CardFooter } from "@chakra-ui/react";

import {
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
} from "@chakra-ui/react";

import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

import { useToast } from "@chakra-ui/react";
import { color } from "framer-motion";
import ToggleColorMode from "./Toggle";
import useSocket from "../hooks/sendData";
// import usePostRequest from "../hook/usePostRequest";
// import ErrorAlertComponent from "../components2/ErrorAlert";
// import SuccessAlertComponent from "../components2/SuccessAlert";

const RegistrationFormFinal = () => {
  const [formfirstName, setFirstName] = useState("");
  // const [value, setValue] = useState("Calibration Form");
  const [showExtraFields, setShowExtraFields] = useState(false);
  const toast = useToast();
  //const { loading, error, data, post } = usePostRequest();
   const [message, setMessage] = useState('');
  const { socket, sendMessage } = useSocket('http://localhost:4000');


  const signupInformationRef = useRef({
    first_name: "",
    last_name: "",
    username: "",
    email: "",
    disabled: false,
    driver_category: "",
    role: "Admin",
    hashed_pwd: "",
  });

  const handleFormReset = (formikProps) => {
    console.log("form reset called");
    formikProps.resetForm();
    // setShowExtraFields(false); // Reset the additional state
  };

  const handleSubmit = (values, actions) => {
    //   console.log("reg", values.firstName);

    console.log("values", values.volume);

    sendMessage('save', { volume: values.volume });
    setMessage('');

    setMessage(values.volume)
    // signupInformationRef.current.first_name = values.firstName;
    // signupInformationRef.current.last_name = values.lastName;
    // signupInformationRef.current.hashed_pwd = values.password;
    // signupInformationRef.current.email = values.email;

    // signupInformationRef.current.username =
    //   values.username !== undefined ? values.username : "empty";
    // signupInformationRef.current.driver_category =
    //   values.category !== undefined ? values.category : "empty";

    // console.log("sign up info:-", signupInformationRef.current);

    //post("http://localhost:8000/register-user", signupInformationRef.current);

    actions.resetForm();
  };


  function handleStartMeasurement(){

    sendMessage('measure', { start: "start_measurement" });
    setMessage('');




  }



  

  const validationSchema = Yup.object().shape({
    // firstName: Yup.string().required("First name is required"),
    // lastName: Yup.string().required("Last name is required"),
    // username: Yup.string().required("Username name is required"),
    // password: Yup.string().required("password is required"),

    volume: Yup.number().required("You need to provide a number.."),
  });

  return (
    <>
      {/* {error && <ErrorAlertComponent errorMessage={"Something Went Wrong"} />} */}

      {/* {data && (
        <SuccessAlertComponent successMessage={"Successfully registered"} />
      )} */}

      <ToggleColorMode />

      <SimpleGrid
        spacing={10}
        templateColumns="repeat(auto-fill, minmax(300px, 1fr))"
        padding={8}
      >
        <Card>
          <CardHeader>
            <Heading mb="10px" size="md">
              {" "}
              Mix 1 Calibration form
            </Heading>

            <Alert status="warning">
              <AlertIcon />
              {/* <AlertTitle>Calibration</AlertTitle> */}
              <AlertDescription>Measure 500ml</AlertDescription>
            </Alert>

            <Box mt={6}>
              <Button onClick={handleStartMeasurement} colorScheme="orange" width="250px">
                Start Measurement
              </Button>
            </Box>
          </CardHeader>
          <CardBody>
            <Formik
              // initialValues={{ firstName: "" }}
              initialValues={{
                volume: "",

                // Add this field with an initial value
              }}
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
            >
              {(formikProps) => (
                <>
                  <Form>
                    <Box>
                      <Field name="volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume">
                              How much did you measure?
                            </FormLabel>
                            <Input
                              {...field}
                              id="volume"
                              placeholder="Volume"
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <HStack>
                      <Button
                        mt={6}
                        colorScheme="teal"
                        isLoading={formikProps.isSubmitting}
                        type="submit"
                        disabled={formikProps.isValid} // Disable the button if the form is not valid
                      >
                        Submit
                      </Button>
                    </HStack>
                  </Form>
                </>
              )}
            </Formik>
          </CardBody>
          {/* <CardFooter>
            <Button>View here</Button>
          </CardFooter> */}
        </Card>

        <Card>
          <CardHeader>
            <Heading mb="10px" size="md">
              {" "}
              Mix 2 Calibration form
            </Heading>

            <Alert status="warning">
              <AlertIcon />
              {/* <AlertTitle>Calibration</AlertTitle> */}
              <AlertDescription>Measure 500ml</AlertDescription>
            </Alert>

            <Box mt={6}>
              <HStack>
                <Button colorScheme="orange" width="350px">
                  Start Measurement
                </Button>
              </HStack>
            </Box>
          </CardHeader>
          <CardBody>
            <Formik
              // initialValues={{ firstName: "" }}
              initialValues={{
                volume: "",

                // Add this field with an initial value
              }}
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
            >
              {(formikProps) => (
                <>
                  <Form>
                    <Box>
                      <Field name="volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume">
                              How much did you measure?
                            </FormLabel>
                            <Input
                              {...field}
                              id="volume"
                              placeholder="Volume"
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <Button
                      mt={6}
                      colorScheme="teal"
                      isLoading={formikProps.isSubmitting}
                      type="submit"
                      disabled={formikProps.isValid} // Disable the button if the form is not valid
                    >
                      Submit
                    </Button>
                  </Form>
                </>
              )}
            </Formik>
          </CardBody>
          {/* <CardFooter>
            <Button>View here</Button>
          </CardFooter> */}
        </Card>

        <Card>
          <CardHeader>
            <Heading mb="10px" size="md">
              {" "}
              Mix 3 Calibration form
            </Heading>

            <Alert status="warning">
              <AlertIcon />
              {/* <AlertTitle>Calibration</AlertTitle> */}
              <AlertDescription>Measure 500ml</AlertDescription>
            </Alert>

            <Box mt={6}>
              <Button colorScheme="orange" width="250px">
                Start Measurement
              </Button>
            </Box>
          </CardHeader>
          <CardBody>
            <Formik
              // initialValues={{ firstName: "" }}
              initialValues={{
                volume: "",

                // Add this field with an initial value
              }}
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
            >
              {(formikProps) => (
                <>
                  <Form>
                    <Box>
                      <Field name="volume">
                        {({ field, form }) => (
                          <FormControl
                            isInvalid={
                              form.errors.volume && form.touched.volume
                            }
                          >
                            <FormLabel htmlFor="volume">
                              How much did you measure?
                            </FormLabel>
                            <Input
                              {...field}
                              id="volume"
                              placeholder="Volume"
                              // value={formfirstName}
                            />{" "}
                            <FormErrorMessage>
                              {form.errors.volume}
                            </FormErrorMessage>
                          </FormControl>
                        )}
                      </Field>
                    </Box>

                    <Button
                      mt={6}
                      colorScheme="teal"
                      isLoading={formikProps.isSubmitting}
                      type="submit"
                      disabled={formikProps.isValid} // Disable the button if the form is not valid
                    >
                      Submit
                    </Button>
                  </Form>
                </>
              )}
            </Formik>
          </CardBody>
          {/* <CardFooter>
            <Button>View here</Button>
          </CardFooter> */}
        </Card>
      </SimpleGrid>

      <Box padding={10} alignContent="right">
        <Button colorScheme="red" width="250px">
          Proceed Without Calibration
        </Button>
      </Box>
    </>
  );
};

export default RegistrationFormFinal;
