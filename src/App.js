import logo from './logo.svg';
import './App.css';
import { ChakraProvider } from '@chakra-ui/react'
import MainComponent from './components/Main';
import theme from "./theme.js";



function App() {
  return (
    <ChakraProvider theme={theme}>
      <MainComponent />
    </ChakraProvider>
    
  );
}

export default App;
